import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SimpleCard from "./SimpleCard";
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

const GalleryView = (props) => {
    const classes = useStyles();

    useEffect(() => {
        // console.log(props)
    })
      return (
        <div className={classes.root}>
        <Grid container spacing={3}>
            {props.filteredPokemon.map((pokemon) => {
                return (
                <Grid item xs key={pokemon.name}>
                    <SimpleCard 
                        key={pokemon.name}
                        name={pokemon.name} 
                        img={pokemon.sprites.front_default} 
                        height={pokemon.height}
                        weight={pokemon.weight}
                        order={pokemon.order}
                    />
                </Grid>
                )
            })}
        </Grid>
        </div>
      )
}

export default GalleryView;