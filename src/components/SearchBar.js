import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

const SearchBar = (props) => {
  const [pokemon, setPokemon] =  useState("");
  
  const handleChange = (e) => {
    setPokemon(e.target.value);
    props.onChange(e.target.value);
  }

  return (
      <input className="myInput"
        value={pokemon}
        onChange={handleChange}
      />
  );
}

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default SearchBar;
