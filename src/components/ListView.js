import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';


function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  
  function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  }
}));

export default function InteractiveList(props) {
  const classes = useStyles();
  const [tall, setTall] = useState(false);
  const [primary, setPrimary] = useState(false);
  const [sort, setSort] = useState(false);
  const [open, setOpen] = useState(false);
  const [modalStyle] = useState(getModalStyle);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              checked={sort}
              onChange={event => {
                  if (sort) {
                    props.pokelist.sort(function(a,b) {
                        return a.height - b.height;
                    });
                  } else {
                    props.pokelist.sort(function(a,b) {
                        return b.height - a.height;
                    });
                  }
                  setSort(event.target.checked)
              }}
              value="sort"
            />
          }
          label="S O R T"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={tall}
              onChange={event => setTall(event.target.checked)}
              value="tall"
            />
          }
          label="T A L L"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={primary}
              onChange={event => setPrimary(event.target.checked)}
              value="flying"
            />
          }
          label="F L Y I N G"
        />
      </FormGroup>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Typography variant="h6" className={classes.title}>
            P O K E M O N
          </Typography>
          <div className={classes.demo}>
            <List>
              {primary ? (tall ? props.pokelist.map((poke) => {
                  return (
                      <ListItem>
                          <ListItemText
                            primary={(poke.types[0].type.name == 'flying' && poke.height > 13) ? poke.name + ' '  + poke.height : null}                      
                          />
                          <Button
                            onClick={handleOpen}
                          > O P E N</Button>
                      </ListItem>
                  )
              }) : props.pokelist.map((poke) => {
                return (
                    <ListItem>
                        <ListItemText 
                          primary={(poke.types[0].type.name == 'flying') ? poke.name + ' '  + poke.height : null} 
                        />
                    </ListItem>
                )
            })) : (tall ? props.pokelist.map((poke) => {
                return (
                    <ListItem>
                        <ListItemText 
                          primary={poke.height > 13 ? poke.name + ' '  + poke.height : null} 
                        />
                    </ListItem>
                )
            }) : props.pokelist.map((poke) => {
                return (
                    <ListItem>
                        <ListItemText 
                          primary={poke.name + ' '  + poke.height} 
                        />
                    </ListItem>
                )
            }) ) }
            </List>
          </div>
        </Grid>
      </Grid>

      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
        >
        <div style={modalStyle} className={classes.paper}>
    <h2 id="simple-modal-title">NAME : {props.name}</h2>
        <p id="simple-modal-description">
            HEIGHT : {props.height}
            <br/>
            WEIGHT : {props.weight}
            <br/>
            ORDER : {props.order}
        </p>
        </div>
        </Modal>
    </div>
  );
}