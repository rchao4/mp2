import React, {useEffect, useState} from 'react';
import SearchBar from "./SearchBar";
import ListView from "./ListView";
import GalleryView from "./GalleryView";
import {HttpClient} from "../api/httpClient";
import Checkbox from '@material-ui/core/Checkbox';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';


import {useDispatch, useSelector} from "react-redux";
import {fetchPokeList} from "../store/pokemon/pokemonActions";

  function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  
  function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }

  const useStyles = makeStyles(theme => ({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));

const MainPage = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const [filteredPokemon, setFilteredPokemon] = useState([]);
    const [checked, setChecked] = useState(false);
    const pokeList = useSelector(state => state.pokeList);
    const [modalStyle] = useState(getModalStyle);

    const [open, setOpen] = useState(false);


    useEffect(() => {
        console.log("MOUNTED");
        
        dispatch(fetchPokeList()).then((response) => 
          console.log(response.data)
        )
    }, []);

    const filterPokemon = (pokemon) => {
        let filteredPokemon = pokeList;
        filteredPokemon = filteredPokemon.filter((poke) => {
            let pokeName = poke.name.toLowerCase()
            return pokeName.indexOf(
                pokemon.toLowerCase()) !== -1;
        })
        let returnProm = Promise.all(filteredPokemon.map((poke) => {
                let pokeResponse = HttpClient.get(poke.url).then((response) => {
                    return (response.data)
                })
                return (pokeResponse) 
        })).then( (response) => {
            setFilteredPokemon(response) 
        }
        )
    }

    const handleOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };

    const handleChange = event => {
        let filtered = filteredPokemon.filter((poke) => poke.height > 12)
        setFilteredPokemon(filtered);
        setChecked(event.target.checked);
    };


    return (
        <div>
            <SearchBar onChange={filterPokemon}/>
            <ListView pokelist={filteredPokemon}/>
            <Checkbox
                checked={checked}
                onChange={handleChange}
                value="primary"
                inputProps={{ 'aria-label': 'primary checkbox' }}
            />
            <GalleryView filteredPokemon={filteredPokemon} />


        </div>
    )
}

export default MainPage;