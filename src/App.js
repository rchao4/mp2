import React, { Component } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import MainPage from "./components/MainPage";

//redux-state
import { Provider } from "react-redux";
import { configureStore } from "./store/configureStore";
const store = configureStore({ test: "testing" });

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Switch>
              <Route exact path="/" component={MainPage} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
