import {combineReducers} from "redux";
import pokeReducer from "./pokemon/pokemonReducer";

const rootReducer = combineReducers({
    pokeList: pokeReducer
})

export default rootReducer;