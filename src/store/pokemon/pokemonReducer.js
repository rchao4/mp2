import {POKEMON_LIST_FULFILL_FETCH} from "./pokemonActions";

export default function pokeReducer(state = [], action) {
    switch(action.type) {
        case POKEMON_LIST_FULFILL_FETCH:
            console.log('PREF FETCH', action.response.data);
            return [...state, ...action.response.data.results];
        default:
            console.log('DEFAULT');
            return state;
    }
} 