import {HttpClient} from "../../api/httpClient";

export const POKEMON_LIST_START_FETCH = "POKEMON_LIST_START_FETCH";
export const POKEMON_LIST_FULFILL_FETCH = "POKEMON_LIST_FULFILL_FETCH";

function pokeStartFetch() {
    return {POKEMON_LIST_START_FETCH};
}

function pokeListFulfillFetch(response) {
    return {
        type: POKEMON_LIST_FULFILL_FETCH,
        response
    };
}

export function fetchPokeList() {
    return dispatch => {
        console.log("add pokemon");
        return HttpClient.get("https://pokeapi.co/api/v2/pokemon").then(response =>
            dispatch(pokeListFulfillFetch(response))
        )
    }
}